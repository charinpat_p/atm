var account = {
    '1234' : {'pin': '1234', 'balance': 500},
    '5678' : {'pin': '5678', 'balance': 1500},
    '0000' : {'pin': '0000', 'balance': 2500}
  };
var accountNo;
$(document).ready(function() {

  $('.form-menu').hide();
  $('.form-dep').hide();
  $('.form-wit').hide();
  $('.form-end').hide();

  $('.form-signin').submit(function() {
    accountNo = $('#accountNo').val();
    var pin = $('#pin').val();
    if (accountNo != null && account[accountNo]['pin'] == pin) {
      $('.form-menu').show();
      $('.form-signin').hide();
      showbalance(accountNo);
    } else {
      alert('เลขที่บัญชีหรือรหัสประจำตัวไม่ถูกต้อง');
    }
    return false;
  })

  $('#dep').click(function() {
    $('.form-menu').hide();
    $('.form-dep').show();
    return false;
  })

  $('.form-dep').submit(function() {
    var amount = $('#deposit').val();
    amount = parseInt(amount,10);

    if (amount >= 0) {
      deposit(accountNo, amount);
      $('.form-dep').hide();
      $('.form-end').show();
      showbalance(accountNo);
    } else {
      alert('กรุณากรอกตัวเลขให้ถูกต้อง (จำนวนเงินไม่สามารถติดลบได้)');
    }
    return false;
  })

  $('#wit').click(function() {
    $('.form-menu').hide();
    $('.form-wit').show();
    return false;
  })

  $('.form-wit').submit(function() {
    var amount = $('#withdraw').val();
    amount = parseInt(amount,10);
    
    if (amount < 0){
      alert('กรุณากรอกตัวเลขให้ถูกต้อง (จำนวนเงินไม่สามารถติดลบได้)');
    }
    else{
      if (amount <= account[accountNo]['balance']) {
        $('.form-wit').hide();
        $('.form-end').show();
        withdraw(accountNo, amount);
        showbalance(accountNo);
      } else {
        alert('ยอดเงินในบัญชีไม่เพียงพอ');
      }
    }
    return false;
  })
  

  function showbalance(accountNo) {
    $(".balance").html(account[accountNo]['balance'].toString())
  }
  function deposit(accountNo, amount) {
    account[accountNo]['balance'] += amount
  }
  function withdraw(accountNo, amount) {
    account[accountNo]['balance'] -= amount
  }
})